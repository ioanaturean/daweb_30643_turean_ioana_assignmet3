export default {


    copy: {

        p3: 'Sunt student în anul patru la Universitatea Tehnică din Cluj-Napoca, Facultatea de Automatică și Calculatoare, Departamentul de Calculatoare. Sunt orientat pe echipă și mă înțeleg cu alții atunci când lucrez într-un cadru de grup. Pot conduce echipa sau pot urma comenzi fără probleme, oricare ar fi nevoie. De asemenea, am capacitatea de a lucra independent, stând la program și respectând acele termene stricte. Cred că sunt un cursant rapid și nu renunț ușor atunci când sunt confruntat cu provocări dificile.',
        p4: '\n' +

            'Posibilitatea clienților de a interacționa cu un calendar care schimbă date în timp real. </h4>\n' +
            '<h4> În prezent, toate datele din programul sălii de fitness sunt statice. </h4>\n' +
            '<h4> O altă posibilă dezvoltare este posibilitatea de a rula aplicația pe mobil\n' +
            'de asemenea. </h4>\n' +
            '<h4> Pentru aceasta, veți avea nevoie de o implementare separată pentru sistemele Android și iOS\n' +
            'suport pentru aplicația dezvoltată\n' +
            '<h4> O caracteristică utilă și importantă pe care ar trebui să o luăm în considerare este a\n' +
            'secțiune în care clientul este informat despre consum\n' +
            'din caloriile pe care le faceți în timpul unui antrenament și stocarea acestor informații în\n' +
            'cerere. </h4>',
        p5: 'Sunt student în anul IV la Universitatea Tehnică din Cluj-Napoca, Facultatea din\n' +
            ' Departamentul de automatizare și informatică, Departamentul de Informatică. Sunt orientat către echipă și obțin\n' +
            'împreună cu alții când lucrați într-un cadru de grup.\n' +
            'Pot conduce echipa sau pot urma comenzi fără probleme, oricare ar fi nevoie. De asemenea, am\n' +
            'capacitatea de a lucra independent în timp ce stai la program\n' +
            'și respectând acele termene stricte.',
        p6: 'Student la Universitatea Tehnică, Facultatea de Automatică și Calculatoare, anul IV',
        p7: ' PLANIFICATOR FITNESS - APLICAȚIE PENTRU SĂLI DE FITNESS  </h3>\n' +
            '<h6> Digitalizarea procesului de programare online pentru una dintre sesiunile de instruire\n' +
            ' sala de fitness le organizează </h6>\n' +
            '<h6> Crearea unei aplicații web pentru orice remediere rapidă o poate face\n' +
            'interacționează cu o sală de fitness pentru a participa la programul oferit de aceasta. </h6>\n' +
            '<h6> Ținând cont de timpul necesar, s-a constatat că astfel\n' +
            'sistemul ar putea adăuga beneficii în ceea ce privește încredințarea acestuia în timp ce pot fi acordate. </h6>\n' +
            '<h6> După finalizarea unui studiu bibliografic, puteți vedea în capitolul următor,\n' +
            ' sunt avute în vedere îngrijirile implicate în căutarea sistemelor de îngrijire și funcționalități similare\n' +
            ' este necesar să adăugați noi funcții pentru un sistem de rezervare online\n' +
            '  mai flexibil și mai accesibil pentru clienți Majoritatea sistemelor îl pot susține\n' +
            ' mai concentrat pe managementul intern al sălilor de fitness și nu este concentrat pe\n' +
            ' CLIENȚI. </h6>\n' +
            ' <h6> Aplicația web dezvoltată în ceea ce privește lucrările este axată în continuare\n' +
            '  managementul intern al angajaților, dar mai mult asupra clientului. Cererea prezentată\n' +
            ' În ceea ce privește lucrările, obiectivul principal este clientul și satisfacerea nevoilor mari\n' +
            '  Despre serviciile de fitness. </h6>',
        p8: '<p> <b> Contextul proiectului: </b> Digitalizarea procesului de programare online la\n' +
            '  unul dintre antrenamentele pe care le organizează sala de sport\n' +
            'Crearea unei aplicații web pentru orice utilizator care dorește\n' +
            ' interacționează cu un centru de fitness pentru a participa la programe\n' +
            'oferit de aceasta.\n' +
            '  Managementul angajaților și instruirea de către un administrator autorizat\n' +
            '  Afișează programul fiecărui antrenor din perioada curentă\n' +
            ' </ P>\n' +
            '\n' +
            '\n' +
            ' <p> <b> Scopul: </b> crearea unei aplicații care oferă utilizatorilor posibilitatea de a-și crea propria lor\n' +
            ' program săptămânal la o sală de fitness specifică, denumită în aplicația beFIT, în funcție de\n' +
            '  programele realizate în cadrul acelui gimnaziu.\n' +
            ' </ P>\n' +
            '\n' +
            ' <p> <b> Obiective: </b>\n' +
            ' Crearea unei interfețe prietenoase și ușor de utilizat\n' +
            ' Asigurarea securității aplicației\n' +
            '\n' +
            ' <q> Sistemul acceptă interacțiunea cu trei tipuri de utilizatori: administrator, antrenor, client\n' +
            ' Posibilitatea clientului de a face o rezervare pentru unul dintre programele disponibile\n' +
            '  Asistență offline pentru aplicația web dezvoltată\n' +
            '  Extensibilitate în ceea ce privește evoluțiile ulterioare </q>\n' +
            '</ P>',
        p10: '<h3> Coordonator: dr. S.I. ing. Ion-Augustin Giosan </h3>\n' +
            ' <h4> Rolul: profesor asistent la Universitatea Tehnică din Cluj-Napoca, Informatică\n' +
            'Departament </ h4>\n' +
            '<h6> Direcția de cercetare: profesor asistent pentru subiecte Prelucrare imagini, model\n' +
            ' Sisteme de recunoaștere, programare computerizată și programare orientată pe obiect la tehnic\n' +
            ' Universitatea din Cluj-Napoca, Departamentul de Informatică\n' +
            'Activități de cercetare în cadrul Grupului de procesare a imaginii și recunoaștere a modelului la\n' +
            ' Universitatea Tehnică din Cluj-Napoca </h6>',
    },

}
