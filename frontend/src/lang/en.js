export default {

    copy: {

        p3: 'I am a fourth-year student at Technical University of Cluj-Napoca, Faculty of Automation and\n' +
            'Computer Science, Computer Science Department.\n' +
            'I am team oriented and get along with others when working in a group setting. I can lead the\n' +
            'team or follow orders without any problems, whichever is needed. I also have the ability to\n' +
            'work independently while staying on schedule and meeting those tight deadlines.\n' +
            'I believe that I am quick learner, and I don\'t give up easily when faced with difficult\n' +
            'challenges.',
        p4: ' The ability for customers to interact with a calendar that changes real-time data. </h4>\n' +
            '  <h4> At present, all the data in the fitness room schedule are static. </h4>\n' +
            '  <h4> Another possible development is the possibility of running the application on mobile\n' +
            ' too.</h4>\n' +
            '<h4> For this you will need a separate implementation for Android and iOS systems to provide\n' +
            ' support for the developed application\n' +
            ' <h4> A useful and important feature that we should consider is a\n' +
            ' section in which the customer is informed about the consumption\n' +
            ' of the calories you make during a workout and storing this information in\n' +
            ' application. </h4>',
        p5: ' I am a fourth-year student at Technical University of Cluj-Napoca, Faculty of\n' +
            'Automation and Computer Science, Computer Science Department.I am team oriented and get\n' +
            'along with others when working in a group setting.\n' +
            'I can lead the team or follow orders without any problems, whichever is needed. I also have\n' +
            ' the ability to work independently while staying on schedule\n' +
            ' and meeting those tight deadlines.',
        p6: 'Student at the Technical University, Faculty of Automation and Computers, year IV',
        p7: ' FITNESS PLANNER – Application for gyms </h3>\n' +
            ' <h6> Digitalization of the online programming process for one of the training sessions\n' +
            '  the fitness room organizes them </h6>\n' +
            '  <h6> Creating a web application for any quick fix can make it\n' +
            '  interact with a fitness room to participate in the program offered by it. </h6>\n' +
            ' <h6> Taking into account the time needed, it has been found that such\n' +
            ' system could add benefits in terms of entrusting it while they can be granted. </h6>\n' +
            '  <h6> After completing a bibliographic study, you can see in the following chapter,\n' +
            ' care involved in looking for care systems and similar functionalities is considered\n' +
            'it is necessary to add new features for an online booking system\n' +
            ' more flexible and more accessible to customers Most systems can support it\n' +
            '  more focused on the internal management of fitness rooms and are not focused on\n' +
            '  CUSTOMERS. </h6>\n' +
            '<h6> The web application developed with regard to the works is further focused\n' +
            'internal management of employees, but more on the client. The application presented\n' +
            ' As far as the works are concerned, the main objective is the client and meeting the big needs\n' +
            'About fitness services. </h6>',
        p9: '  <h5> Time has become very important in a world of turmoil, a world in which\n' +
            '  we want all things we can do as quickly as possible, to be informed in\n' +
            ' permanence of the things that interest us and we can access this information from\n' +
            '  wherever we are. Particular attention must be paid to a lifestyle\n' +
            '  healthy, maintaining a healthy mind and a healthy body. To be\n' +
            ' always be informed about specialized health maintenance programs\n' +
            ' optimal, it would be beneficial to use systems to provide this information\n' +
            '  at any time. </h5>\n' +
            '    <h5> The presented system is a web application designed for any user\n' +
            '   wants to interact with a fitness room to participate in the programs offered\n' +
            '    of this. </h5>\n' +
            '  <h5> Efficiency of the process consists of the possibility to make reservations\n' +
            ' online, without having to travel to the fitness center to make sure\n' +
            ' the fact that the desired program will take place this week or that the places\n' +
            '   available were not occupied. </h5>\n' +
            '   <h5> Thus, using the web application, the user\n' +
            '  you can be constantly informed about the programs that take place in the hall\n' +
            ' respective fitness, can follow the services provided by the gym or can enter\n' +
            ' direct contact with the employed persons, having all the necessary information accessible.\n' +
            ' the bitch provides support to the fitness room manager who is responsible for\n' +
            ' adding, deleting or editing new employees or new workouts available. </h5>\n' +
            ' <h5> The coach has access to the application through a page intended for him that he accesses in\n' +
            ' depending on the role you play and may be aware of all the trainings on\n' +
            '  which has them during the current day, the current week or the current month. The one of\n' +
            'the third role that the application supports is the client role. </h5>\n' +
            ' <h5> The client can\n' +
            '  make reservations online depending on the availability of places, they can see\n' +
            ' reservations, it has pages designed to inform you about all the services you have\n' +
            ' the gym offers them, about the program of the room and the programs that are accessible\n' +
            'during the current period. </h5>\n' +
            '  <h5> The application comes with a very important feature, which makes it different from other\n' +
            'applications\n' +
            '  similarly developed so far, namely the possibility of being used in the mode\n' +
            ' offline, with all the resources needed to run it, saved in a special memory. </h5>',

        p8: ' <p><b> Project context: </b> Digitalization of the online programming process at\n' +
            ' one of the workouts that the gym organizes\n' +
            ' Creating a web application for any user who wants to\n' +
            ' interact with a fitness center to participate in the programs\n' +
            ' offered by it.\n' +
            '  Management of employees and training by an authorized administrator\n' +
            ' Display the program of each coach from the current period\n' +
            ' </p>\n' +
            '\n' +
            '\n' +
            '  <p><b> Purpose: </b> to create an application that gives users the opportunity to create their own\n' +
            ' weekly program at a specific fitness room, named in the beFIT application, depending on\n' +
            '  the programs realized within that gymnasium.\n' +
            ' </p>\n' +
            '\n' +
            ' <p><b> Objectives: </ b>\n' +
            ' Creating a user-friendly and user-friendly interface\n' +
            ' Ensuring the security of the application\n' +
            '\n' +
            '   <q> The system supports interaction with three types of users: administrator, coach, client\n' +
            ' The possibility of the client to make a reservation for one of the available programs\n' +
            'Offline support for the developed web application\n' +
            '  Extensibility with regard to further developments </q>\n' +
            '  </p>',
        p10: ' <h3>Coordonator: S.I dr. ing. Ion-Augustin Giosan </h3>\n' +
            ' <h4>Role: Assistant Professor at Technical University of Cluj-Napoca, Computer Science\n' +
            ' Department</h4>\n' +
            ' <h6>Research direction: Assistant Professor for the subjects Image Processing, Pattern\n' +
            '  Recognition Systems, Computer Programming and Object Oriented Programming at the Technical\n' +
            'University of Cluj-Napoca, Computer Science Department\n' +
            ' Research activities within the Image Processing and Pattern Recognition Group at the\n' +
            ' Technical University of Cluj-Napoca</h6>',
    },


}
