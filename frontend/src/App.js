import React, {Component,} from "react";
import './App.css';
import UploadScreen from "./login/UploadScreen";
import {Layout, Header, Navigation, Drawer, Content} from 'react-mdl';
import Main from './components/main';
import {Link} from 'react-router-dom';
import counterpart from 'counterpart';
import Translate from 'react-translate-component';
import en from './lang/en';
import de from './lang/de';
import * as locales from "./lang";
import LoginScreen from './login/Loginscreen';

import NavLink from "react-bootstrap/NavLink";
import Route from "react-router-dom/Route";

Object.keys(locales).forEach(key => {
    console.log(key, locales, [key]);
    counterpart.registerTranslations(key, locales[key]);
});
counterpart.registerTranslations('en', en);
counterpart.registerTranslations('de', de);
counterpart.setLocale('en');

class App extends Component {
    state = {
        lang: localStorage.getItem('lang')
    }

    componentDidMount() {
        const {lang} = this.state;
        counterpart.setLocale(lang);
    }

    onLangChange = e => {
        localStorage.setItem('lang', e.target.value)//store the selected language to localStorage.
        this.setState({lang: e.target.value});//switch the state
        counterpart.setLocale(e.target.value);
    }

    constructor(props){
        super(props);
        this.state={
            loginPage:[],
            uploadScreen:[]
        }
    }
    componentWillMount(){
        var loginPage =[];
        loginPage.push(<LoginScreen appContext={this} key={"login-screen"}/>);
        this.setState({
            loginPage:loginPage
        })
    }

    render() {

        return (
            <div className="demo-big-content">
                <select value={this.state.lang} onChange={this.onLangChange}>
                    <option value="en">EN</option>
                    <option value="de">RO</option>
                </select>
                <Translate/>

                <Layout>
                    <Header className="header-color"
                            title={<Link style={{textDecoration: 'none', color: 'white'}} to="/">Home</Link>} scroll>

                        <Navigation>
                            {localStorage.getItem("token") && <Link to="/" onClick={function () {
                                localStorage.removeItem("token")
                            }}>Logout</Link>}
                            {!localStorage.getItem("token") && <Link to="/login">Login</Link>}
                            {localStorage.getItem("token") && <Link to="/profile">Profile </Link>}
                            <Link to="/resume">Resume </Link>
                            <Link to="/news">News </Link>
                            <Link to="/desprelucrare">About </Link>
                            <Link to="/contact">Contact Me </Link>


                        </Navigation>
                    </Header>
                    <Drawer title={<Link style={{textDecoration: 'none', color: 'black'}} to="/">MyPortfolio</Link>}>
                        <Navigation>
                            <Link to="/profile">Profile </Link>
                            <Link to="/resume">Resume</Link>
                            <Link to="/news">Noutati</Link>
                            <Link to="/desprelucrare">Projects</Link>
                            <Link to="/contact">Contact</Link>


                        </Navigation>
                    </Drawer>
                    <Content>
                        <div className="page-content"/>
                        <Main/>
                    </Content>
                </Layout>
            </div>

       );
    }
}

export default App;
