import React, {Component} from 'react';
import {Grid, Cell} from 'react-mdl';
import Translate from 'react-translate-component';
import axios from 'axios';

class Profile extends Component {

    constructor(props){
        super(props);
        this.state = {
            'name': '',
            'email': '',
            'inputN': '',
            'inputE': ''
        }
    }

    // shouldComponentUpdate(nextProps, nextState) {
    //     if (this.state.inputN != nextState.inputN || this.state.inputE != nextState.inputE) {
    //         return false;
    //     }
    //     return true;
    // }

    componentWillMount() {
        let config = {
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + localStorage.getItem("token")
            }
        };
        axios.get("http://127.0.0.1:8000/api/details", config).then((response) => {
            this.setState( {
                'name': response.data.success.name,
                'email': response.data.success.email
            });
        })
    }

    sendData(key) {
        let value;
        if (key === "name") {
            value = this.state.inputN;
        } else {
            value = this.state.inputE;
        }
        let config = {
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + localStorage.getItem("token")
            }
        };
        axios.post("http://127.0.0.1:8000/api/details", {[key]: value}, config).then((response) => {
            this.setState( {
                'name': response.data.success.name,
                'email': response.data.success.email
            });
        })
    }

    handleChangeE = event => {
        this.setState({'inputE': event.target.value})
    };

    handleChangeN = event => {
        this.setState({'inputN': event.target.value})
    };

    render() {
        return (
            <div>
                <Grid>
                    <Cell col={4}>
                        <div style={{textAlign: 'left'}}>
                            <img
                                src="https://fbcd.co/images/avatars/36d794f897dff519419ccb6c1af6291b.png"
                                alt="avatar"
                                style={{height: '200px'}}
                            />
                            {/*<button type="button">Upload Image</button>*/}
                        </div>

                        <h2 style={{paddingTop: '2em'}}>{this.state.name}</h2>
                        <input type="text" value={this.state.inputN} onChange={this.handleChangeN} id="name" name="name"/>
                        <button type="button" onClick={() => {this.sendData("name")}}>Edit</button>
                        <h4 style={{color: 'grey'}}>Programmer</h4>
                        <hr style={{borderTop: '3px solid #833fb2', width: '50%'}}/>

                        {/*<Translate content="copy.p3" component="p" className="class" unsafe={true}/>*/}

                        <h5>Email</h5>
                        <p>{this.state.email}</p>
                        <input id="email" type="text" value={this.state.inputE} onChange={this.handleChangeE} name="email"  />
                            <button type="button" onClick={() => {this.sendData("email")}}>Edit!</button>
                    </Cell>

                </Grid>
            </div>
        )
    }
}

export default Profile;
