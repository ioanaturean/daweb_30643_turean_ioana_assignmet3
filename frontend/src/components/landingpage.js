import React, {Component} from 'react';
import {Grid, Cell} from 'react-mdl';
import Translate from "react-translate-component";

class Landing extends Component {
    render() {
        return (
            <div style={{width: '100%', margin: 'auto'}}>
                <Grid className="landing-grid">
                    <Cell col={12}>
                        <img
                            src="https://fbcd.co/images/avatars/36d794f897dff519419ccb6c1af6291b.png"
                            alt="avatar"
                            className="avatar-img"
                        />

                        <div className="banner-text">
                            <h1>Ioana Turean</h1>

                            <Translate content="copy.p6" component="p" className="class" unsafe={true}/>

                            <hr/>

                            <p>Java | Bootstrap | JavaScript | React | React Native | NodeJS | Selenium | Cucumber</p>

                            <div className="social-links">

                                {/* LinkedIn */}
                                <a href="https://www.linkedin.com/in/ioana-turean-1a9b23168/" rel="noopener noreferrer"
                                   target="_blank">
                                    <i className="fa fa-linkedin-square" aria-hidden="true"/>
                                </a>

                                {/* Github */}
                                <a href="https://github.com/ioanaturean/" rel="noopener noreferrer" target="_blank">
                                    <i className="fa fa-github-square" aria-hidden="true"/>
                                </a>

                                {/* Freecodecamp */}
                                <a href="https://www.freecodecamp.org/" rel="noopener noreferrer" target="_blank">
                                    <i className="fa fa-free-code-camp" aria-hidden="true"/>
                                </a>

                                {/* Youtube */}
                                <a href="https://www.youtube.com/" rel="noopener noreferrer" target="_blank">
                                    <i className="fa fa-youtube-square" aria-hidden="true"/>
                                </a>

                            </div>
                        </div>
                    </Cell>
                </Grid>
            </div>
        )
    }
}

export default Landing;
