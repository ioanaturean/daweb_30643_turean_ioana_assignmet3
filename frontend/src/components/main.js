import React from 'react';

import LandingPage from './landingpage';
import AboutMe from './aboutme';
import Contact from './contact';
import Projects from './projects';
import Resume from './resume';
import Login from '../login/Loginscreen';
import Profile from './profile';

import {Switch, Route} from "react-router-dom";
// import SignInForm from "./SignInForm";
// import Profile from "./profile";


const Main = () => (

    <Switch>
        <Route exact path="/" component={LandingPage}/>
        <Route path="/login" component={Login}/>
        <Route path="/news" component={AboutMe}/>
        <Route path="/contact" component={Contact}/>
        <Route path="/desprelucrare" component={Projects}/>
        <Route path="/resume" component={Resume}/>
        <Route path="/profile" component={Profile}/>
    </Switch>

)

export default Main;
