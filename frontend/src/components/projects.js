import React, {Component} from 'react';
import {Tabs, Tab, Grid, Cell, Card, CardTitle, CardText, CardActions, Button, CardMenu, IconButton} from 'react-mdl';
import Translate from "react-translate-component";
import axios from "axios";


class Projects extends Component {
    constructor(props) {
        super(props);
        this.state = {activeTab: 0, comments: [], inputN: '', inputC: ''};
    }

    getComments = () => {
        let config = {
            headers: {
                "Content-Type": "application/json",
            }
        };
        axios.get("http://127.0.0.1:8000/api/comments", config).then((response) => {
            this.setState( {
                'comments': response.data.data
            });
        })
    };

    componentDidMount() {
        this.getComments();
    }

    toggleCategories() {

        if (this.state.activeTab === 1) {
            return (
                <div className="projects-grid">
                    {/* Project 1 */}
                    <Card shadow={5} style={{minWidth: '450', margin: 'auto'}}>
                        <CardTitle style={{
                            color: '#4B0082',
                            height: '176px',
                            background: 'url(https://images.ctfassets.net/2y9b3o528xhq/5YhXXuS0hIw6JV3nJr3GgP/682bf2a70a98c3e466f26c2c2a812d65/front-end-web-developer-nanodegree--nd001.jpg) center / cover'
                        }}>Frontend #1</CardTitle>
                        <CardText>
                            Front-end web development is the practice of converting data to a graphical interface,
                            through the use of HTML, CSS, and JavaScript, so that users can view and interact with that
                            data.
                        </CardText>
                        <CardActions border>
                            <Button colored>GitHub</Button>
                            <Button colored>CodePen</Button>
                            <Button colored>Live Demo</Button>
                        </CardActions>
                        <CardMenu style={{color: '#fff'}}>
                            <IconButton name="share"/>
                        </CardMenu>
                    </Card>

                    {/* Project 2 */}
                    <Card shadow={5} style={{minWidth: '450', margin: 'auto'}}>
                        <CardTitle style={{
                            color: '#fff',
                            height: '176px',
                            background: 'url(https://skillvalue.com/jobs/wp-content/uploads/sites/7/2019/12/back-end-developer-freelancing-project-remote-available.png) center / cover'
                        }}>Backend #2</CardTitle>
                        <CardText>
                            Backend development languages handle the ‘behind-the-scenes’ functionality of web
                            applications. It’s code that connects the web to a database, manages user connections, and
                            powers the web application itself.
                        </CardText>
                        <CardActions border>
                            <Button colored>GitHub</Button>
                            <Button colored>CodePen</Button>
                            <Button colored>Live Demo</Button>
                        </CardActions>
                        <CardMenu style={{color: '#fff'}}>
                            <IconButton name="share"/>
                        </CardMenu>
                    </Card>

                    {/* Project 3 */}
                    <Card shadow={5} style={{minWidth: '450', margin: 'auto'}}>
                        <CardTitle style={{
                            color: '#4B0082',
                            height: '176px',
                            background: 'url(https://www.supinfo.com/articles/resources/214348/5678/0.png) center / cover'
                        }}>Database #3</CardTitle>
                        <CardText>
                            A database is just like a room in an office where all the files and important info can be
                            stored related to a project.
                            The info that we store can be very sensitive so we always have to be careful while accessing
                            or manipulating the info in the database.
                        </CardText>
                        <CardActions border>
                            <Button colored>GitHub</Button>
                            <Button colored>CodePen</Button>
                            <Button colored>Live Demo</Button>
                        </CardActions>
                        <CardMenu style={{color: '#fff'}}>
                            <IconButton name="share"/>
                        </CardMenu>
                    </Card>
                </div>

            )
        } else if (this.state.activeTab === 0) {
            return (
                <div><h3><i> </i></h3>
                    <Translate content="copy.p7" component="p" className="class" unsafe={true}/>

                    <div style={{textAlign: 'center'}}>
                        <img
                            src="https://vivasportclub.ro/wp-content/uploads/2019/09/ffw_site-893x350.jpg"
                            alt="avatar"
                            style={{height: '200px'}}
                        />
                    </div>

                </div>
            )
        } else if (this.state.activeTab === 2) {
            return (
                <div>
                    <p>
                        <div style={{textAlign: 'left'}}>
                            <img
                                src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQWSFD6urz4HinXq6GtyJ8okebrGi5UPfhSG_AIaLrBZrAE7NAS"
                                alt="avatar"
                                style={{height: '200px'}}
                            />
                        </div>

                        <Translate content="copy.p7" component="p" className="class" unsafe={true}/>
                    </p>

                </div>
            )
        } else if (this.state.activeTab === 3) {
            return (
                <div>
                    <Translate content="copy.p8" component="p" className="class" unsafe={true}/>

                    <div style={{textAlign: 'center'}}>
                        <img
                            src="https://previews.123rf.com/images/nomadsoul1/nomadsoul11802/nomadsoul1180200245/95366702-fat-sweaty-woman-using-dumbbells-in-gym.jpg"
                            alt="avatar"
                            style={{height: '200px'}}
                        />
                    </div>
                </div>

            )
        } else if (this.state.activeTab === 4) {
            return (
                <div>
                    <p>
                        <h6>
                            <h6>1] Frisbie Matt, Angular 2 Cookbook, Packt Publishing, January 2017. </h6>
                            <h6>2] Jérôme Jaglale, Spring Cookbook, Packt Publishing 2015. </h6>
                            <h6>3] Nate Murray, ng-book The complete book on Angular 4., Gumroad, October 2016. </h6>
                            <h6>4] Paul Fisher, Persistence with Spring, Apress, 2010. </h6>
                            <h6> 5] Sudha Belida Balaji Varanasi, Spring Rest, Apress, March 30, 2014.</h6>
                            <h6> 6] Kathy Sierra Bert Bates, Head First Of Java, Reilly, 2003. </h6>
                            <h6>7] Jonathan Knudsen Patrick Niemeyer, Learning Java, O’ Reilly, 2000. </h6>
                            <h6>8] Maarten Van Steen Andrew S. Tanenbaum, Distributed Systems- Principles and Paradigms.
                                Amsterdam, The Netherlands, Prentice Hall, 2006. </h6>
                            <h6>9] Ioan Salomie, curs Sisteme Distribuite http://dsrl.coned.utcluj.ro/ </h6>
                            <h6>10] Ionut Anghel, curs Dezvoltarea aplicatiilor web
                                http://users.utcluj.ro/~ianghel/DAW/ </h6>
                            <h6>11] Cosmina Ivan,curs Baze de date http://users.utcluj.ro/~civan/ </h6>
                            <h6>12] Angular 2, https://angular.io/tutorial </h6>
                            <h6>13] Spring Framework, https://spring.io/ </h6>
                            <h6>14] Offline suport, https://www.html5rocks.com/en/tutorials/appcache/beginner/ </h6>
                            <h6>15] Spring Data JPA,
                                https://docs.spring.io/spring-data/jpa/docs/current/reference/html/ </h6>
                            <h6>16] Three Tier Architecture,
                                https://www.genuitec.com/spring-frameworkrestcontroller-vs-controller/ </h6>
                        </h6>
                    </p>
                </div>
            )
        } else if (this.state.activeTab === 5) {
            return (
                <div>
                    <p>
                        <Translate content="copy.p10" component="p" className="class" unsafe={true}/>

                    </p>
                </div>)
        }

    }

    renderComments() {
         let data = [];
         this.state.comments.forEach(el => {
             data.push(<table><tr><th><b>{el.author}</b></th></tr><tr><td>{el.content}</td></tr></table>);
         });
         return data;
    }

     handleChangeC = event => {
         this.setState({'inputC': event.target.value})
     };

     handleChangeN = event => {
         this.setState({'inputN': event.target.value})
     };

    handleSubmit = event => {
        let config = {
            headers: {
                "Content-Type": "application/json",
            }
        };
        axios.post("http://127.0.0.1:8000/api/comments", {author: this.state.inputN, content: this.state.inputC}, config)
            .then(() => {this.getComments()});
        event.preventDefault();
    };

    render() {
        return (
            <div>
                <Tabs activeTab={this.state.activeTab} onChange={(tabId) => this.setState({activeTab: tabId})} ripple>
                    <Tab>Titlu</Tab>
                    <Tab>Structura</Tab>
                    <Tab>Descriere</Tab>
                    <Tab>Obiective</Tab>
                    <Tab>Referinte</Tab>
                    <Tab>Coordonator</Tab>
                </Tabs>

                <Grid>
                    <Cell col={12}>
                        <div className="content">{this.toggleCategories()}</div>
                    </Cell>
                </Grid>

                <div>
                    {this.renderComments()}
                </div>
                <br />
                <hr />

                {localStorage.getItem("token") && <form onSubmit={this.handleSubmit}>
                    <label>
                        Name:
                        <input type="text" value={this.state.inputN} onChange={this.handleChangeN} />
                    </label>
                    <br />
                    <br />
                    <label>Comment: </label>
                    <textarea value={this.state.inputC} onChange={this.handleChangeC} />
                    <input type="submit" value="Submit" />
                </form>}

            </div>
        )
    }
}

export default Projects;
