import React, {Component} from 'react';
import {Grid, Cell} from 'react-mdl';
import Education from './education';
import Experience from './experience';
import Skills from './skills';
import Translate from 'react-translate-component';

class Resume extends Component {

    render() {
        return (
            <div>
                <Grid>
                    <Cell col={4}>
                        <div style={{textAlign: 'center'}}>
                            <img
                                src="https://fbcd.co/images/avatars/36d794f897dff519419ccb6c1af6291b.png"
                                alt="avatar"
                                style={{height: '200px'}}
                            />
                        </div>

                        <h2 style={{paddingTop: '2em'}}>Ioana Turean</h2>
                        <h4 style={{color: 'grey'}}>Programmer</h4>
                        <hr style={{borderTop: '3px solid #833fb2', width: '50%'}}/>

                        <Translate content="copy.p3" component="p" className="class" unsafe={true}/>

                        <hr style={{borderTop: '3px solid #833fb2', width: '50%'}}/>
                        <h5>Address</h5>
                        <p>Mun. Cluj-Napoca, Calea Bucuresti, nr. 66, Bl. D18, et, II, ap.8</p>
                        <h5>Phone</h5>
                        <p>0741-988-241</p>
                        <h5>Email</h5>
                        <p>ioanaturean97@gmail.com</p>
                        <h5>Web</h5>
                        <p>https://www.linkedin.com/in/ioana-turean-1a9b23168/</p>
                        <hr style={{borderTop: '3px solid #833fb2', width: '50%'}}/>
                    </Cell>
                    <Cell className="resume-right-col" col={8}>
                        <h2>Education</h2>


                        <Education
                            startYear={2016}
                            endYear={2020}
                            schoolName="UTCN, Faculty of Automation and Computers"
                            schoolDescription="Courses studied: Mathematical Analysis, Linear Algebra, Procedural Programming,
                                Object Oriented Programming, Structure of Computing Systems, Distributed Systems
                                Languages used: JAVA, C, C ++, VHDL, Python, OpenCV "
                        />

                        <Education
                            startYear={2012}
                            endYear={2016}
                            schoolName="Liceul Teoretic PAVEL DAN Campia-Turzii"
                            schoolDescription="Profile: Non-intensive mathematics-computer science"
                        />
                        <hr style={{borderTop: '3px solid #e22947'}}/>

                        <h2>Experience</h2>

                        <Experience
                            startYear={2019}
                            endYear={2020}
                            jobName="Second Job"
                            jobDescription="Software Engineer @Endava Romania"
                        />

                        <Experience
                            startYear={2018}
                            endYear={2019}
                            jobName="First Job"
                            jobDescription="Junior Software Developer @Evolving Systems BLS"
                        />
                        <hr style={{borderTop: '3px solid #e22947'}}/>
                        <h2>Skills</h2>
                        <Skills
                            skill="java"
                            progress={60}
                        />
                        <Skills
                            skill="HTML/CSS"
                            progress={30}
                        />
                        <Skills
                            skill="NodeJS"
                            progress={100}
                        />
                        <Skills
                            skill="React"
                            progress={100}
                        />

                    </Cell>
                </Grid>
            </div>
        )
    }
}

export default Resume;
