import React, {Component} from 'react';
import {Grid} from 'react-mdl';
import Translate from "react-translate-component";

class About extends Component {

    render() {

        return (
            <div>
                <Grid className="about-grid">

                    <h1><b>NEWS </b></h1>
                    <Translate content="copy.p4" component="p" className="class" unsafe={true}/>

                </Grid>

            </div>
        )
    }
}

export default About;
