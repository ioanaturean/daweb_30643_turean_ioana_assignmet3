import React, {Component} from 'react';
import {Grid, Cell, List, ListItem, ListItemContent} from 'react-mdl';
import Translate from "react-translate-component";

class Contact extends Component {
    render() {
        return (
            <div className="contact-body">
                <Grid className="contact-grid">
                    <Cell col={6}>
                        <h2>Ioana Turean</h2>
                        <img
                            src="https://fbcd.co/images/avatars/36d794f897dff519419ccb6c1af6291b.png"
                            alt="avatar"
                            style={{height: '250px'}}
                        />
                        <p style={{width: '75%', margin: 'auto', paddingTop: '1em'}}>

                            <Translate content="copy.p5" component="p" className="class" unsafe={true}/>

                        </p>

                    </Cell>
                    <Cell col={6}>
                        <h2>Contact Me</h2>
                        <hr/>

                        <div className="contact-list">
                            <List>
                                <ListItem>
                                    <ListItemContent style={{fontSize: '30px', fontFamily: 'Anton'}}>
                                        <i className="fa fa-phone-square" aria-hidden="true"/>
                                        0741988241
                                    </ListItemContent>
                                </ListItem>

                                <ListItem>
                                    <ListItemContent style={{fontSize: '30px', fontFamily: 'Anton'}}>
                                        <i className="fa fa-fax" aria-hidden="true"/>
                                        0264-365-002
                                    </ListItemContent>
                                </ListItem>

                                <ListItem>
                                    <ListItemContent style={{fontSize: '30px', fontFamily: 'Anton'}}>
                                        <i className="fa fa-envelope" aria-hidden="true"/>
                                        ioanaturean97@gmail.com
                                    </ListItemContent>
                                </ListItem>

                                <ListItem>
                                    <ListItemContent style={{fontSize: '30px', fontFamily: 'Anton'}}>
                                        <i className="fa fa-skype" aria-hidden="true"/>
                                        ioana_em19
                                    </ListItemContent>

                                </ListItem>

                            </List>

                            <form action="http://localhost:5000/result" method="get">

                                TO: <input type="text" name="email"/>
                                Subject: <input type="text" name="subject"/>
                                Body: <input type="text" name="body"/>

                                <input type="submit" value="SEND MAIL"/>
                            </form>
                        </div>
                    </Cell>
                </Grid>
            </div>
        )
    }
}

export default Contact;
